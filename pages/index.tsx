import React from "react";
import Login from "./login";

export default function Home() {
  return (
    <>

      <Login />
      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: "Kanit", sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </>
  );
}
